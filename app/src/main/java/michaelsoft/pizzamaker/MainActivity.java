package michaelsoft.pizzamaker;

import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import org.w3c.dom.Text;

import michaelsoft.pizzamaker.pizza.FactoryPizza;
import michaelsoft.pizzamaker.pizza.IFactory;
import michaelsoft.pizzamaker.pizza.IPizza;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private int itemSelected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        itemSelected = 0;

        Spinner spinner = (Spinner) findViewById(R.id.spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.TiposPizza, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(this);
    }

    public void preparar(View view){

        IPizza pizza;

        switch (itemSelected){
            case 0:
                pizza = new FactoryPizza().getPizza(IFactory.tipo.jamonQueso);
                break;
            case 1:
                pizza = new FactoryPizza().getPizza(IFactory.tipo.vegetariana);
                break;
            default:
                pizza = new FactoryPizza().getPizza(IFactory.tipo.jamonQueso);
                break;
        }

        TextView txt = (TextView) findViewById(R.id.textView);
        txt.setText(pizza.obtenerIngredientes());
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {
        itemSelected = pos;
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
