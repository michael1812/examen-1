package michaelsoft.pizzamaker.pizza;

/**
 * Created by Michael on 11/10/2017.
 */

public interface IFactory {

    enum tipo {
        jamonQueso, vegetariana
    }

    public IPizza getPizza(tipo pTipo);
}
