package michaelsoft.pizzamaker.pizza;

/**
 * Created by Michael on 11/10/2017.
 */

public class PizzaVegetariana implements IPizza{

    private int tamaño;
    private int cantSlices;
    private String ingredientes;
    private boolean preparada;

    private PizzaVegetariana(){
        tamaño = 0;
        cantSlices= 0;
        ingredientes = "";
        preparada = false;
    }

    @Override
    public IPizza preparar(int pTamaño, int pCantSlices) {
        tamaño = pTamaño;
        cantSlices = pCantSlices;
        ingredientes = "vegetales";
        return this;
    }

    @Override
    public String obtenerIngredientes() {
        return ingredientes;
    }

    public static class builder{
        private int tamaño;
        private int cantSlices;

        public builder(){
        }

        public builder setTamaño(int pTamaño){
            tamaño = pTamaño;
            return this;
        }

        public builder setcantSlices(int pCantSlices){
            cantSlices = pCantSlices;
            return this;
        }

        public IPizza build(){
            return new PizzaVegetariana().preparar(tamaño, cantSlices);
        }
    }
}
