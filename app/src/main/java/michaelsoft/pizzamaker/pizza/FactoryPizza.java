package michaelsoft.pizzamaker.pizza;

/**
 * Created by Michael on 11/10/2017.
 */

public class FactoryPizza implements IFactory {
    @Override
    public IPizza getPizza(tipo pTipo) {

        IPizza pizza;

        switch (pTipo){
            case jamonQueso:
                pizza = new PizzaJamonQueso
                        .builder()
                        .setTamaño(3)
                        .setcantSlices(8).build();
                break;
            case vegetariana:
                pizza = new PizzaVegetariana
                        .builder()
                        .setTamaño(3)
                        .setcantSlices(8).build();
                break;
            default:
                pizza = new PizzaJamonQueso
                        .builder()
                        .setTamaño(3)
                        .setcantSlices(8).build();
                break;
        }

        return pizza;
    }
}
