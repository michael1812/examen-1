package michaelsoft.pizzamaker.pizza;

/**
 * Created by Michael on 11/10/2017.
 */

public interface IPizza {

    public IPizza preparar(int pTamaño, int pCantSlices);
    public String obtenerIngredientes();
}
